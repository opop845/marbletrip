﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileObject : MonoBehaviour
{
    public int title;
    public int index;
    public void Init(int tile,int index )
    {
        title = tile;
        this.index =index;
    }
}
