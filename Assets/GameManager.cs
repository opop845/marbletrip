﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Color[] colors;

    public Player player;

    public TileObject[] tiles;

    private List<int> list = new List<int>();
    private List<TileObject> listTile = new List<TileObject>();
    public DOTweenAnimation tween;
    public int step = -1;

    public bool isGameOver = false;
    public bool isStart = false;
    public GameObject gameOver;
    public TextMeshProUGUI textScore;
    public bool EditorMode;
    public Slider hp;
    public Image imageHandler;

    public Transform tileParent;

    private float gravity = -9.8f;
    public float accel = 0f;

    public float speed = 1f;

    private int tileStep = 0;
    private float value2 = 4f;
    private void Awake()
    {
        instance = this;
        for (int i = 0; i < 20; i++)
        {
            AddTile();
        }
        hp.value = 0.5f;
    }

    public void AddTile()
    {
        var value = Random.Range(0, colors.Length);
        tileStep++;
        int randomValue = Random.Range(0, tiles.Length);
        var obj = Instantiate(tiles[randomValue], tileParent);
        obj.transform.localPosition = new Vector3(0f, (tileStep + 1) * 0.63f, 0f);
        obj.Init(tileStep, randomValue);
    }


    public void GameStart()
    {
isStart = true;
    }


    public void OnClickUp()
    {
        player.currentIndex = selectColor;
        player.OnClickAttack();
    }

    private void Update()
    {
        if (isStart)
        {
            accel += (gravity * Time.deltaTime);
            tileParent.Translate(0f, accel * Time.deltaTime * speed, 0f);
            // var temp = Mathf.Clamp(step / 10, 1, 6);
            // hp.value -= Time.deltaTime * (0.1f * temp);
            // if (hp.value <= 0f)
            //     GameOver();
        }

        if (Input.GetKeyDown(KeyCode.A))
            OnClickUp();

    }

    private int selectColor;
    public void OnClickColor(int index)
    {
        selectColor = index;
        imageHandler.color = colors[index];
    }

    public void GameOver()
    {
        isGameOver = true;
        gameOver.SetActive(true);
    }

    public void OnClickRestart()
    {
        SceneManager.LoadScene(0);
    }
}
