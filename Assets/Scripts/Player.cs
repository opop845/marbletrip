﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : CacheBehaviour
{
  public  int currentIndex = 0;

    public void OnClickAttack(){
        GameManager.instance.GameStart();
        rigidbody2D.AddForce(Vector2.up*50f);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        var tileObject=  other.gameObject.GetComponent<TileObject>();
        if(tileObject != null)
        {
            if(tileObject.index == currentIndex)
            Destroy(tileObject.gameObject);
            GameManager.instance.AddTile();
            GameManager.instance.accel *= 0.5f;
        }
    }


}
