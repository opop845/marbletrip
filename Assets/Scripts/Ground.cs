﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.GetComponent<TileObject>() != null)
        GameManager.instance.GameOver();
    }
}
